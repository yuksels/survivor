#ifndef ALIVE_H
#define ALIVE_H
#include <QCoreApplication>

	class Alive
	{

	private:
		QString name;
		int hp;
		int ad;
		QString statusIsEnemy;
		int position;

	public:


		Alive()
		{
			name="";
			hp=0;
			ad=0;
		}
		void SetAd(int ad)
		{
			this->ad=ad;
		}
		void SetHp(int hp)
		{
			this->hp=hp;
		}
		void SetPosition(int position)
		{
			this->position=position;
		}
		void SetName(QString name)
		{
			this->name=name;
		}
		QString GetName()
		{
			return this->name;
		}
		int GetPosition()
		{
			return this->position;
		}
		int GetHp()
		{
			return this->hp;
		}
		int GetAd()
		{
			return this->ad;
		}
		QList<Alive> Versus(Alive Enemy,Alive Ally) //hero ve bir enemy nin çatışması
		{
			QList<Alive> fighterList;

			while(true)
			{
				Enemy.hp=Enemy.hp-Ally.ad;
				Ally.hp=Ally.hp-Enemy.ad;

				if(Ally.hp<=0 || Enemy.hp<=0)// h
				{
					Ally.position=Enemy.position;
					fighterList.append(Enemy);
					fighterList.append(Ally);
					return fighterList;
				}
			}
		}


	};


#endif // ALIVE_H
