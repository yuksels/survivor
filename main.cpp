#include <QCoreApplication>
#include"iostream"
#include<QList>
#include<QDebug>
#include<qfile.h>
#include<qdir.h>
#include <QFile>
#include <QTextStream>
#include<alive.h>

// global değişkenler ve listeler
QString line ;
int recourcesPosition;
QList<Alive> nesneListesi,geciciListe;

void ParseText(QString txt)
{

	 QStringList satirListesi= txt.split("\n");
	 foreach (QString satir, satirListesi)
	 {

		 Alive Enemy;
		 int hp,ad;
		 QStringList kelimeListesi = satir.split(" ");
		if(satir.isEmpty()==false && kelimeListesi[2]!="Enemy" && kelimeListesi[0]!="Resources" && kelimeListesi[0]!="There")// bütün canlıların ad ve hp leri alınıp bir class a atanır ve  geici listede tutulur
		{

			if(kelimeListesi[2]!="is") //hp at
			{
				hp=kelimeListesi[2].toInt();


			}
			if(kelimeListesi[2]=="is") //ad ata
			{
				Enemy.SetName(kelimeListesi[0]); // isim ata
				Enemy.SetHp(hp);
				ad=kelimeListesi[3].toInt();
				Enemy.SetAd(ad);
				if(kelimeListesi[0]=="Hero")
				{
					Enemy.SetPosition(0);
					nesneListesi.append(Enemy);
				}
				geciciListe.append(Enemy);
			}
		}
	 }
	 foreach (QString satir, satirListesi)
	{
		Alive dusman;

		QStringList kelimeListesi=satir.split(" ");
		if(kelimeListesi[0]=="There")
		{

			int i;
			QString adi= kelimeListesi[3];// name
			int position=kelimeListesi[6].toInt();//positon
			for(i=1; i<geciciListe.count();i++)
			{
				if(geciciListe[i].GetName()==adi)
				{
					dusman.SetAd(geciciListe[i].GetAd());
					dusman.SetHp(geciciListe[i].GetHp());
					dusman.SetName(adi);
					dusman.SetPosition(position);
					nesneListesi.append(dusman);
				}
			}
		}
	 }
}
void Sirala() // bubble sıralama
{
	Alive temp;
	for(int i=1;i<nesneListesi.count();i++)
	{
		for(int j=1;j<nesneListesi.count()-i;j++)
		{

			if(nesneListesi[j].GetPosition()>nesneListesi[j+1].GetPosition())
			{
						temp=nesneListesi[j];
						nesneListesi[j]=nesneListesi[j+1];
						nesneListesi[j+1]=temp;
			}
		}
	}
}
void ReadFile()
{
QFile file("/home/samet/qtproject/Survivor/AlivesInfo.txt");

if(!file.open(QIODevice::ReadOnly))
{
	qDebug() << "error opening file: " << file.error();
	return;
}
QTextStream instream(&file);
line = instream.readAll();file.close();
return;
}


int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	int i;
	ReadFile();
	Alive tmp;
	QList<Alive> fighterList;
	ParseText(line);

	Sirala();
	qDebug()<<"Hero started journey with"<<nesneListesi[0].GetHp()<<"HP!";
	for(i=1;i<nesneListesi.count();i++)
	{

		//qDebug()<<nesneListesi[i].GetName()<<".."<< nesneListesi[i].GetPosition();

		fighterList=tmp.Versus(nesneListesi[i],nesneListesi[0]);
		nesneListesi[0]=fighterList[1];
		// fighterList[0] ==>> enemy fighterList[1] ==>> ally
		if(fighterList[1].GetHp()<=0)// enemy kaznmışsa
		{

			qDebug()<<fighterList[0].GetName()<<" defeated Hero with "<<fighterList[0].GetHp()<<"HP remaining";
			qDebug()<<"Hero is Dead!! Last seen at position "<<fighterList[0].GetPosition()<<"!!";
			break;
		}
		if( fighterList[0].GetHp()<=0) //hero kazanmışsa
		{
			qDebug()<<"Hero defeated "<<fighterList[0].GetName()<<" with "<<fighterList[1].GetHp()<<"HP remaining";
			if(i==nesneListesi.count()-1)
			{
				qDebug()<<"Hero survived!!";
			}
		}
	//qDebug()<<"versus"<<nesneListesi.count()<<fighterList[0].GetName()<<", ad:"<<fighterList[0].GetAd()<<",hp:"<<fighterList[0].GetHp()<<",position:"<<fighterList[0].GetPosition()<<fighterList[0].GetIsEnemy();

	}
	/*for(i=0;i<geciciListe.count();i++)
	{
		qDebug()<<geciciListe[i].GetName()<<", ad:"<<geciciListe[i].GetAd()<<",hp:"<<geciciListe[i].GetHp()<<",position:"<<geciciListe[i].GetPosition()<<geciciListe[i].GetIsEnemy();
	}
	qDebug()<<"\n";
	for(i=0;i<nesneListesi.count();i++)
	{
		qDebug()<<i<<nesneListesi[i].GetName()<<", ad:"<<nesneListesi[i].GetAd()<<",hp:"<<nesneListesi[i].GetHp()<<",position:"<<nesneListesi[i].GetPosition()<<nesneListesi[i].GetIsEnemy();
	}*/

	return a.exec();
}
